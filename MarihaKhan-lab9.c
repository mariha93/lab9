#include<stdio.h>
#include<stdlib.h>

typedef (*vtable)(struct Matrix *m);

vtable *tb_m;
vtable *tb_v;

typedef struct Matrix {
	int row;
	int column;
	int *array;
	void (*Matrix)( struct Matrix *matrix, int row, int col);
	void (*multiply)( struct Matrix *matrix1, struct Matrix *matrix2, struct Matrix *resultant);
	void (*add)( struct Matrix *matrix1, struct Matrix *matrix2, struct Matrix *resultant);
	vtable *table_m;
}Matrix;

typedef struct Vector{
	Matrix matrix;
	int max;
	int min;
	void (*vec)( struct vector *matrix1, int row, int col);
	vtable *table_v;
}vector;

int L1_norm_matrix(Matrix *matrix){
	int sum=0;
	int final=0;
	int i,j =0;
	for(j=0; j<matrix->column; j++){
		for(i=0; i<(matrix->row); i++){
			sum=sum+matrix->array[j*matrix->column +i];
		}
		if(sum>final){
			final=sum;
		}
	}
	return final;
}

int L1_norm_vector(Matrix *matrix){
	int sum=0;
	int final=0;
	int i=0;
	for(i=0; i<(matrix->row); i++){
		sum=sum+matrix->array[i];
	}
	return sum;
}

void add(Matrix *matrix1, Matrix *matrix2 , Matrix *resultant){
	resultant->array=(int *)malloc( (matrix1->row*matrix1->column)* sizeof(int));
	int i=0;
	for(i=0;i<(matrix1->column*matrix1->row); i++){
		resultant->array[i]=matrix1->array[i]+matrix2->array[i];
	}
}


void multiply(Matrix *matrix1, Matrix *matrix2, Matrix *resultant){
	int j=0;
	int sum=0;
	int f=0;
	int k = 0;
	resultant->array=(int *)malloc( (matrix1->row*matrix2->column)* sizeof(int));
	int i, l =0;
	for (i = 0; i<(matrix1->row*matrix1->column); i = i + matrix1->column){
		j = i;
		f = 0;
		for (l = 0; l<(matrix2->row*matrix2->column);){
			sum = sum + (matrix1->array[j] * matrix2->array[l]);
 			l = l + matrix2->column;
			j++;
			if ((j) % matrix1->column == 0){
				resultant->array[k] = sum;
				j = i;
				if (l != ((matrix2->row*matrix2->column)-1) + matrix2->column){
					f++;
					l = f;
				}
				k++;
				sum=0;
			}
		}
	}
}

void Matrix_init( Matrix *matrix, int row, int col){
	matrix->multiply=multiply;	
	matrix->add=add;
	matrix->row= row;
	matrix->column=col;
	matrix->array=(int *)malloc(sizeof(int)*(row*col));
	int i=0;
	for(i=0;i<(row*col); i++){
		matrix->array[i] = 2;
	}
}

void V_Add(vector *v1, vector *v2 , vector *resultant_vector){
	resultant_vector->matrix.array=(int *)malloc( (v1->matrix.row*v1->matrix.column)* sizeof(int));
	int i=0;
	for(i=0;i<(v1->matrix.column*v1->matrix.row); i++){
		resultant_vector->matrix.array[i]=v1->matrix.array[i]+v2->matrix.array[i];
	}
}

void Vector_init( vector *vector, int row, int col){
	vector->matrix.add=V_Add;
	vector->matrix.row= row;
	vector->matrix.column=col;
	vector->matrix.array=(int *)malloc(sizeof(int)*(row*col));
	int i=0;
	for(i=0;i<(row*col); i++){
		vector->matrix.array[i] = 2;
	}
}

int main(){

	int row_matrix1 = 5;
	int col_matrix1 = 5;
	int row_matrix2 = 5;
	int col_matrix2 = 5;
	int row_vector1 = 5;
	int row_vector2 = 5;
	
	Matrix matrix1;
	Matrix matrix2;
	Matrix resultant;
	Matrix resultant2;
	vector vector1;
	vector vector2;
	vector resultant_vector;


	matrix1.Matrix=Matrix_init;
	matrix1.Matrix(&matrix1, row_matrix1,col_matrix1);
	matrix2.Matrix=Matrix_init;
	matrix2.Matrix(&matrix2, row_matrix2,col_matrix2);

	matrix2.multiply(&matrix1,&matrix2, &resultant2);


	printf("Result of multiplication of two matrices:\n");
	int i, j =0;
	for(i=0; i<row_matrix1;i++){
		for (j=0; j<col_matrix2; j++){
			printf("%d  ", resultant2.array[i*matrix2.column +j]);
		}
		printf("\n");
	}

	matrix2.add(&matrix1, &matrix2,&resultant);
	printf("Result of addition of two matrices:\n");
	for(i=0; i<matrix2.row;i++){
		for (j=0; j<matrix2.column; j++){
			printf("%d  ", resultant.array[i*matrix2.column +j]);
		}
		printf("\n");
	}


	tb_m=(vtable *)malloc( (10)* sizeof(vtable));
	tb_m[0]=L1_norm_matrix;
	matrix1.table_m=tb_m;
	matrix2.table_m=tb_m;
	printf("L1 norm of first matrix is: %d\n",matrix1.table_m[0](&matrix1));
	printf("L1 norm of second matrix is: %d\n",matrix2.table_m[0](&matrix2));
	

	vector1.vec=Vector_init;
	vector1.vec(&vector1, row_vector1, 1);
	vector2.vec=Vector_init;
	vector2.vec(&vector2, row_vector2, 1);


	vector1.matrix.add(&vector1, &vector2, &resultant_vector);
	printf("Result of addition of vector is:\n");
	for(i=0; i<vector2.matrix.row;i++){
		printf("%d  \n", resultant_vector.matrix.array[i]);
	}

	tb_v=(vtable *)malloc( (10)* sizeof(vtable));
	tb_v[0]=L1_norm_vector;
	vector1.table_v=tb_v;
	vector2.table_v=tb_v;
	printf("L1 norm of first vector is %d\n",vector1.table_v[0](&vector1.matrix));
	printf("L1 norm of second vector is %d\n",vector2.table_v[0](&vector2.matrix));

}
